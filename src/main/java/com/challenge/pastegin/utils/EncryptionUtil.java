package com.challenge.pastegin.utils;

import com.google.crypto.tink.Aead;
import lombok.SneakyThrows;

public class EncryptionUtil {

    @SneakyThrows
    public static byte[] encrypt(String content, String key, Aead aead) {
        return aead.encrypt(content.getBytes(), key.getBytes());
    }

    @SneakyThrows
    public static String decrypt(byte[] content, String key, Aead aead) {
        return new String(aead.decrypt(content, key.getBytes()));
    }
}
