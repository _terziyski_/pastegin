package com.challenge.pastegin.config;

import com.google.crypto.tink.Aead;
import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadConfig;
import com.google.crypto.tink.aead.AeadFactory;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@AllArgsConstructor
@Configuration
public class BeanConfiguration {

    @Bean
    @SneakyThrows
    public Aead aead() {
        AeadConfig.register();
        KeysetHandle keysetHandle = KeysetHandle.generateNew(
                AeadKeyTemplates.AES256_GCM);

        return AeadFactory.getPrimitive(keysetHandle);
    }
}
