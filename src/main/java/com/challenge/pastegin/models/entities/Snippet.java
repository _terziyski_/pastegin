package com.challenge.pastegin.models.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "pg", name = "snippets")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Snippet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "content")
    private byte[] content;

    @Column(name = "reference")
    private String reference;

    @Column(name = "is_temporary")
    private boolean isTemporary;
}
