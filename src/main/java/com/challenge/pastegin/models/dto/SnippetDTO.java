package com.challenge.pastegin.models.dto;

import lombok.Data;

@Data
public class SnippetDTO {

    private String content;
    private Boolean isTemporary;
}
