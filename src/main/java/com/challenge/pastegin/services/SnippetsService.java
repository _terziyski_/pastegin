package com.challenge.pastegin.services;

import com.challenge.pastegin.models.entities.Snippet;

public interface SnippetsService {

    Snippet getById(Long id);

    Snippet create(String content);

    Snippet getByReference(String reference);

    void delete(Snippet snippet);
}
