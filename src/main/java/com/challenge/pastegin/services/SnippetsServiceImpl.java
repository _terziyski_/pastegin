package com.challenge.pastegin.services;

import com.challenge.pastegin.config.BeanConfiguration;
import com.challenge.pastegin.models.entities.Snippet;
import com.challenge.pastegin.repositories.SnippetsRepository;
import com.challenge.pastegin.utils.EncryptionUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SnippetsServiceImpl implements SnippetsService {

    private final SnippetsRepository snippetsRepository;
    private final BeanConfiguration beanConfiguration;

    @Override
    public Snippet getById(Long id) {
        return snippetsRepository.getById(id);
    }

    @Override
    public Snippet create(String content) {
        Snippet snippet = new Snippet();
        String reference = RandomStringUtils.randomAlphanumeric(8);
        byte[] encryptedContent = EncryptionUtil.encrypt(content, reference, beanConfiguration.aead());
        snippet.setContent(encryptedContent);
        snippet.setReference(reference);
        return snippetsRepository.save(snippet);
    }

    @Override
    public Snippet getByReference(String reference) {
        return snippetsRepository.getByReference(reference);
    }

    @Override
    public void delete(Snippet snippet) {
        snippetsRepository.delete(snippet);
    }
}
