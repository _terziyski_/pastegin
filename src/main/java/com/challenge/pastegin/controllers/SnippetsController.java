package com.challenge.pastegin.controllers;

import com.challenge.pastegin.config.BeanConfiguration;
import com.challenge.pastegin.models.dto.SnippetDTO;
import com.challenge.pastegin.models.entities.Snippet;
import com.challenge.pastegin.services.SnippetsService;
import com.challenge.pastegin.utils.EncryptionUtil;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class SnippetsController {

    private final SnippetsService snippetsService;
    private final BeanConfiguration beanConfiguration;

    private static final String LOCALHOST_ADDRESS = "https://pastegin.onrender.com/";

    @GetMapping()
    public String homePage(Model model) {
        model.addAttribute("snippetDTO", new SnippetDTO());
        return "submit";
    }

    @Transactional
    @PostMapping("/submit")
    public String create(Model model, @ModelAttribute("snippetDTO") SnippetDTO snippetDTO) {
        Snippet snippet = snippetsService.create(snippetDTO.getContent());
        if (snippetDTO.getIsTemporary().equals(true)) {
            snippet.setTemporary(true);
        }
        String decryptedContent = EncryptionUtil.decrypt(snippet.getContent(), snippet.getReference(), beanConfiguration.aead());
        model.addAttribute("content", decryptedContent);
        model.addAttribute("link", LOCALHOST_ADDRESS + snippet.getReference());
        return "snippet";
    }

    @GetMapping("/{reference}")
    public String getByReference(@PathVariable String reference, Model model) {
        Snippet snippet = snippetsService.getByReference(reference);
        if (snippet == null) {
            return "not_found";
        }

        String decryptedContent = EncryptionUtil.decrypt(snippet.getContent(), reference, beanConfiguration.aead());
        model.addAttribute("content", decryptedContent);
        if (snippet.isTemporary()) {
            snippetsService.delete(snippet);
        }

        return "snippet";
    }
}