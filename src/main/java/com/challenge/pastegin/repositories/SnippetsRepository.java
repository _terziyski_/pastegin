package com.challenge.pastegin.repositories;

import com.challenge.pastegin.models.entities.Snippet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SnippetsRepository extends CrudRepository<Snippet, Long> {

    @Query("Select s FROM Snippet s WHERE s.id = :id")
    Snippet getById(Long id);

    @Query("SELECT s FROM Snippet s WHERE s.reference = :reference")
    Snippet getByReference(String reference);

    @Override
    void delete(Snippet snippet);
}
