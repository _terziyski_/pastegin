package com.challenge.pastegin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PasteGinApplication {

    public static void main(String[] args) {
        SpringApplication.run(PasteGinApplication.class, args);
    }

}
